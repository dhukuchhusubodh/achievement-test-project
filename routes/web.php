<?php

use App\Events\CommentWritten;
use App\Events\LessonWatched;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AchievementsController;
use App\Models\Comment;
use App\Models\Lesson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

Route::get('/comment-achievement', function(){

    Auth::loginUsingId(1);
    $comment = Comment::firstOrCreate(
        ['body' => 'Comment '.rand(1,100)],
        ['user_id' => Auth::id()]
    );
    event(new CommentWritten($comment));
});


Route::get('/lesson-achievement/{id}', function($id){

    Auth::loginUsingId(1);
    Auth::user()->lessons()->attach($id, ['watched' => true]);
    $lesson = Lesson::find($id);
    event(new LessonWatched($lesson, Auth::user()));
});


Route::get('/users/{user}/achievements', [AchievementsController::class, 'index']);


