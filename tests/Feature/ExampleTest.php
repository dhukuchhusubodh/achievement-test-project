<?php

namespace Tests\Feature;

use App\Contracts\CommentAchievement;
use App\Contracts\LessonAchievement;
use App\Models\Comment;
use App\Models\Lesson;
use Tests\TestCase;
use App\Models\User;
use App\Services\BadgeService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;

class ExampleTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_example()
    {
        $user = User::factory()->create();

        $response = $this->get("/users/{$user->id}/achievements");

        $response->assertStatus(200);
    }

    public function test_if_achievements_is_set()
    {
        $commentAchievements = config('achievement.comments');
        $this->assertGreaterThan(
            0,
            count($commentAchievements),
            "Comment Achievements are not Set"
        );

        $lessonsAchievements = config('achievement.lessons');
        $this->assertGreaterThan(
            0,
            count($lessonsAchievements),
            "Lesson Achievements are not Set"
        );

        $badges = config('achievement.badges');
        $this->assertGreaterThan(
            0,
            count($badges),
            "Badges are not Set"
        );
    }

    public function test_comment_achievement()
    {
        $user = User::factory()->create();
        Auth::loginUsingId($user->id);
        $this->assertTrue(Auth::check());

        Comment::factory()->count(5)->create(['user_id' => Auth::id()]);

        $commentAchievements = new CommentAchievement();
        $totalCommentAchievements = $commentAchievements->getAchievementCount();
        $this->assertEquals(3, $totalCommentAchievements);

    }

    public function test_watch_achievement()
    {
        $user = User::factory()->create();
        Auth::loginUsingId($user->id);
        $this->assertTrue(Auth::check());

        Lesson::factory()->count(5)->create();
        $attach = [
                    1 => ['watched' => true],
                    2 => ['watched' => true],
                    3 => ['watched' => true],
                    4 => ['watched' => true],
                    5 => ['watched' => true]
        ];

        Auth::user()->lessons()->attach($attach);

        $lessonAchievement = new LessonAchievement();
        $totalLessonAchievements = $lessonAchievement->getAchievementCount();
        $this->assertEquals(2, $totalLessonAchievements);

    }


    public function test_total_achievement()
    {
        $user = User::factory()->create();
        Auth::loginUsingId($user->id);
        $this->assertTrue(Auth::check());

        Comment::factory()->count(5)->create(['user_id' => Auth::id()]);

        Lesson::factory()->count(10)->create();
        $attach = [
                    6 => ['watched' => true],
                    7 => ['watched' => true],
                    8 => ['watched' => true],
                    9 => ['watched' => true],
                    10 => ['watched' => true]
        ];
        Auth::user()->lessons()->attach($attach);

        $lessonAchievement = new LessonAchievement();
        $totalLessonAchievements = $lessonAchievement->getAchievementCount();
        $this->assertEquals(2, $totalLessonAchievements);
        $this->assertEquals("10 Lessons Watched", $lessonAchievement->getNextAchievement());

        $commentAchievement = new CommentAchievement();
        $totalCommentAchievements = $commentAchievement->getAchievementCount();
        $this->assertEquals(3, $totalCommentAchievements);
        $this->assertEquals("10 Comment Written", $commentAchievement->getNextAchievement());

        $totalAchievements = $totalLessonAchievements + $totalCommentAchievements;
        $this->assertEquals(5, $totalAchievements);

        $badge = new BadgeService($lessonAchievement->getAchievements(), $commentAchievement->getAchievements());
        $this->assertEquals("Intermediate", $badge->getCurrentBadge());
        $this->assertEquals("Advanced", $badge->getNextBadge());
        $this->assertEquals(3, $badge->getRemainingAchievementToUnlockNextBadge());

    }
}
