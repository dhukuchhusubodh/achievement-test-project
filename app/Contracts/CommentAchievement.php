<?php

namespace App\Contracts;

use App\Contracts\AchievementInterface;
use App\Events\AchievementUnlocked;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

/**
 *
 */
class  CommentAchievement implements AchievementInterface
{
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    private $user;
    /**
     * @var \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $achievements;
    /**
     * @var
     */
    private $totalCount = 0;

    /**
     *
     */
    public function __construct()
    {
        $this->user = Auth::user();
        $this->achievements = config('achievement.comments');
        $user = $this->user::withCount('comments')->first();
        $this->totalCount = $user->comments_count;
    }

    /**
     * @return mixed|void
     */
    public function checkAchievementUnblock(){
        if (array_key_exists($this->totalCount, $this->achievements)) {
            $commentAchievementsTitle = $this->achievements[$this->totalCount];
            event(new AchievementUnlocked($this->user, $commentAchievementsTitle));
        }
    }

    /**
     * @return Array
     */
    public function getAchievements() :Array{
        return getCompleted($this->achievements, $this->totalCount);
    }

    /**
     * @return String
     */
    public function getNextAchievement() :String{
        return getNext($this->achievements,$this->totalCount);
    }

    /**
     * @return Int
     */
    public function getAchievementCount() :Int{
        return getCompletedCount($this->achievements,$this->totalCount);
    }

}
