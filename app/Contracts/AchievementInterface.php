<?php

namespace App\Contracts;


/**
 *
 */
interface AchievementInterface
{
    /**
     * @return mixed
     */
    public function checkAchievementUnblock();

    /**
     * @return Array
     */
    public function getAchievements() :Array;

    /**
     * @return String
     */
    public function getNextAchievement() :String;

    /**
     * @return Int
     */
    public function getAchievementCount() :Int;

}
