<?php
if (! function_exists('getCompleted')) {
    /**
     * @param array $datas
     * @param Int $total
     * @return array
     */
    function getCompleted(Array $datas, Int $total ) :Array
    {
        $completed = [];
        foreach($datas as $number => $data){
            if($total >= $number){
                array_push($completed, $data);
            }
        }
        return $completed;
    }
}


if (! function_exists('getNext')) {
    /**
     * @param array $datas
     * @param Int $total
     * @return String
     */
    function getNext(Array $datas, Int $total ): String
    {
        ksort($datas, SORT_NUMERIC );
        $next = "";
        foreach($datas as $number => $data){
            if($total < $number){
                $next = $data;
                break;
            }
        }
        return $next;
    }
}

if (! function_exists('getCurrent')) {
    /**
     * @param array $datas
     * @param Int $total
     * @return String
     */
    function getCurrent(Array $datas, Int $total) :String
    {
        ksort($datas, SORT_NUMERIC );
        $current = "";
        foreach($datas as $number => $data){
            if($total >= $number){
                $current = $data;
            }
        }

        return $current;
    }
}

if (! function_exists('getRemainingToUnblockNext')) {
    /**
     * @param array $datas
     * @param Int $total
     * @return Int
     */
    function getRemainingToUnblockNext(Array $datas, Int $total ) :Int
    {
        ksort($datas, SORT_NUMERIC );
        $nextNumber = 0;
        foreach($datas as $number => $badge){
            if($total < $number){
                $nextNumber = $number;
                break;
            }
        }

        return $nextNumber - $total;
    }
}

if (! function_exists('getCompletedCount')) {
    /**
     * @param array $datas
     * @param Int $total
     * @return Int
     */
    function getCompletedCount(Array $datas, Int $total ) :Int
    {
        $completedTotal = 0;
        foreach($datas as $number => $title){
            if($total >= $number){
                $completedTotal++;
            }
        }

        return $completedTotal;
    }
}



