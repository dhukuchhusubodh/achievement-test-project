<?php

namespace App\Services;

use App\Contracts\AchievementInterface;
use App\Events\BadgeUnlocked;
use Illuminate\Support\Facades\Auth;

/**
 *
 */
class BadgeService
{
    /**
     * @var array
     */
    private $completedAchievement;
    /**
     * @var \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $badges;

    /**
     * @param $completedLesson
     * @param $completedComment
     */
    public function __construct($completedLesson, $completedComment){
        $this->badges = config('achievement.badges');
        $this->completedAchievement = array_merge($completedLesson, $completedComment);
    }

    /**
     * @return String
     */
    public function getCurrentBadge() :String{
        return getCurrent($this->badges, count($this->completedAchievement));
    }

    /**
     * @return String
     */
    public function getNextBadge() :String{
        return getNext($this->badges, count($this->completedAchievement));
    }

    /**
     * @return Int
     */
    public function getRemainingAchievementToUnlockNextBadge() :Int{
        return getRemainingToUnblockNext($this->badges, count($this->completedAchievement));
    }

}
