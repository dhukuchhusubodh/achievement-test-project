<?php

namespace App\Services;

use App\Contracts\AchievementInterface;
use App\Contracts\CommentAchievement;
use App\Contracts\LessonAchievement;
use App\Events\BadgeUnlocked;
use Illuminate\Support\Facades\Auth;

/**
 *
 */
class AchievementService
{
    /**
     * @var AchievementInterface
     */
    private $achievementInterface;

    /**
     * @param AchievementInterface $achievementInterface
     */
    public function __construct(AchievementInterface $achievementInterface){
        $this->achievementInterface = $achievementInterface;
    }

    /**
     *
     */
    public function unblockAchievement(){

        $user = Auth::user();
        $this->achievementInterface->checkAchievementUnblock($user);

        $lessonAchievement = new LessonAchievement();
        $totalLessonAchievements = $lessonAchievement->getAchievementCount();

        $commentAchievements = new CommentAchievement();
        $totalCommentAchievements = $commentAchievements->getAchievementCount();

        $totalAchievements = $totalLessonAchievements + $totalCommentAchievements;
        $badges = config('achievement.badges');

        if (array_key_exists($totalAchievements, $badges)) {
            $badgesTitle = $badges[$totalAchievements];
            event(new BadgeUnlocked($user, $badgesTitle));
        }
    }

    /**
     * @return Array
     */
    public function getAchievements() :Array {
        return $this->achievementInterface->getAchievements();
    }

    /**
     * @return String
     */
    public function getNextAchievement() :String {
        return $this->achievementInterface->getNextAchievement();
    }

}
