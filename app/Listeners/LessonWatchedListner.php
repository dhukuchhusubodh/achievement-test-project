<?php

namespace App\Listeners;

use App\Contracts\LessonAchievement;
use App\Events\LessonWatched;
use App\Services\AchievementService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

/**
 *
 */
class LessonWatchedListner
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LessonWatched  $event
     * @return void
     */
    public function handle(LessonWatched $event)
    {
       $achievement = new AchievementService(new LessonAchievement());
       $achievement->unblockAchievement();
    }
}
