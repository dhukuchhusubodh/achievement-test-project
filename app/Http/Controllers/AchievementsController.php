<?php

namespace App\Http\Controllers;

use App\Contracts\CommentAchievement;
use App\Contracts\LessonAchievement;
use App\Models\User;
use App\Services\AchievementService;
use App\Services\BadgeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 *
 */
class AchievementsController extends Controller
{
    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(User $user)
    {
        Auth::loginUsingId(1);
        $lessonAchievement = new AchievementService(new LessonAchievement);
        $commentAchievement = new AchievementService(new CommentAchievement);
        $badge = new BadgeService($lessonAchievement->getAchievements(), $commentAchievement->getAchievements());
        return response()->json([
            'unlocked_achievements' => array_merge($lessonAchievement->getAchievements(), $commentAchievement->getAchievements()),
            'next_available_achievements' => [$lessonAchievement->getNextAchievement(), $commentAchievement->getNextAchievement()],
            'current_badge' => $badge->getCurrentBadge(),
            'next_badge' => $badge->getNextBadge(),
            'remaing_to_unlock_next_badge' => $badge->getRemainingAchievementToUnlockNextBadge()
        ]);
    }
}
