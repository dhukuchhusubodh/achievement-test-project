<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BadgeUnlocked
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $badge_name;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, String $badgesTitle)
    {
        $this->user = $user;
        $this->badge_name = $badgesTitle;

        dump($this->user, $this->badge_name);
    }


}
