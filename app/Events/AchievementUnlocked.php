<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AchievementUnlocked
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $achievement_name;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, String $commentAchievementsTitle)
    {
        $this->user = $user;
        $this->achievement_name = $commentAchievementsTitle;

        dump($this->user, $this->achievement_name);
    }

}
