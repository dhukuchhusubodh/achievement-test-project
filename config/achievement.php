<?php

use Illuminate\Support\Str;

return [

    'lessons' => [
       1 => "First Lesson Watched",
       5 => "5 Lessons Watched",
       10 => "10 Lessons Watched",
       25 => "25 Lessons Watched",
       50 => "50 Lessons Watched",
    ],

    'comments' => [
        1 => "First Comment Written",
        3 => "3 Comments Written",
        5 => "5 Comments Written",
        10 => "10 Comment Written",
        20 => "20 Comment Written",
     ],

     'badges' => [
        0 => "Beginner",
        4 => "Intermediate",
        8 => "Advanced",
        10 => "Master",
     ],

];
