# Project Info #
This is a test project.

### How do I get set up? ###

* Clone the project from repository
* Run command composer install
* Make database and update in .env file and phpunit.xml
* Run command php artisan migrate
* Run command php artisan db:seed
* For visual test I have hit siteURL/comment-achievement for firing comment event and /lesson-achievement/{id} for lesson Event
